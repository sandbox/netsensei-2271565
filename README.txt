
-- SUMMARY --

View All Modes module extends Display Suite with an extra overview that shows
which view modes are in use (custom settings = TRUE) per entity type / per 
bundle.

This is a developer module. Disable this when you deploy to production.

For a full description of the module, visit the project page:
  https://drupal.org/project/2271565
To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2271565

-- REQUIREMENTS --

* Display Suite


-- INSTALLATION --

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 
  for further information.


-- CONFIGURATION --

* None


-- USAGE --

* Go to admin > structure > Display Suite
* Select the "View all modes" tab below from the "Displays" tab.


-- CONTACT --

Current maintainers:
* Matthias Vandermaesen (netsensei) - https://drupal.org/user/350460
